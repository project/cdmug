<?php

// Include the definition of zen_settings() and zen_theme_get_default_settings().
include_once './' . drupal_get_path('theme', 'zen') . '/theme-settings.php';


/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @return
 *   A form array.
 */
function cdmug_settings($saved_settings) {

  // Get the default values from the .info file.
  $defaults = zen_theme_get_default_settings('STARTERKIT');

  // Merge the saved variables and their default values.
  $settings = array_merge($defaults, $saved_settings);

  /*
   * Create the form using Forms API: http://api.drupal.org/api/6
   */
  $form = array();
  /* -- Delete this line if you want to use this setting
  $form['STARTERKIT_example'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use this sample setting'),
    '#default_value' => $settings['STARTERKIT_example'],
    '#description'   => t("This option doesn't do anything; it's just an example."),
  );
  // */

  // Open lyrics.txt
  $lyrics_file = drupal_get_path('theme', 'cdmug') . "/lyrics.txt";
  $fh = fopen($lyrics_file,"r"); // File handler
  $lyrics = fread($fh, filesize($lyrics_file)); // Crappy way of opening.
  
  // Default settings
  $defaults = array(
    'splender_lyrics' => $lyrics,
  );
  
  // Merge default and saved settings. Saved settings override defaults.
  $settings = array_merge($defaults, $saved_settings);
  
  // Add lyrics to theme settings
  $form['splender_lyrics'] = array(
    '#type' => 'textarea',
    '#title' => t('Lyrics'),
    '#default_value' => $settings['splender_lyrics'],
  );
  
  // Add the base theme's settings.
  $form += zen_settings($saved_settings, $defaults);

  // Remove some of the base theme's settings.
  unset($form['themedev']['zen_layout']); // We don't need to select the base stylesheet.

  // Return the form
  return $form;
}
